# README #

### What is this repository for? ###

These are the R and setting files used in the simulation part of the master thesis: *Adapting marketing mix modelling for the retail marketing environment – A road map for development*.

### How do I get set up? ###

The simulation is based on the [Google AMSS](https://github.com/google/amss). You can install the package in R with the command `devtools::install_github("google/amss")`. You also need some other packages to run the full simulation.

**data folder**

The `data` folder contains the settings for the base data set and the improvement scenarios. 

| Folder    | Settings for               |
|-----------|----------------------------|
| orig.para | Base data set              |
| bmmm      | Traditional MMM            |
| geommm    | Category-Region-level MMM |
| prommm    | Product-level MMM          |

Each of the folder contains the following files:

| Setting file                     | Explanation                               |
|----------------------------------|-------------------------------------------|
| factor_df.csv                    | Basic marketing factors for each product  |
| media_cost_seasonality.csv       | The seasonality of media cost             |
| media_investment_seasonality.csv | Investment emphasis over time             |
| prices.csv                       | Product prices over time                  |
| region_factors.csv               | Region specific factors                   |
| search.csv                       | Search timing sheet                       |
| tv.csv                           | TV timing sheet                           |
| seasonalities.csv                | Base product seasonalities                |
| *.rds                            | the full setting file for the Google AMSS |

**R scripts**

The simulation has 5 basic scripts. 

| Script                            | Explanation                                                                                                                        |
|-----------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| setting_generation.R              | Includes a function to read the setting files from the data folder and then to create a complete setting file for the Google AMSS. |
| basedata_simulation.R             | Simulates the base data set with its counterfactuals and stores the result in a RDS file.                                          |
| extract_data.R                    | Gathers the relevant information from the base RDS file for analysis.                                                              |
| improvement_scenario_simulation.R | Simulates the situations with the improvements implemented (with counterfactuals).                                                 |
| print_results.R                   | Reads all the RDS files and prints out the improvements in profit and ROAS.                                                        |

### Author ###

Antti Heliste, School of Science, Aalto University