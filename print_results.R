# -------- Print improvements -----------
# Compares the changes in profits and ROAS between scenarios

library(tidyverse)

location.names <- c("origI.rds" ,"bmmmI.rds", "geommmI.rds", "prommmI.rds")

profits <- c(0)

final.year.start <- 1.5*52*7+1
final.year.end <- 2.5*52*7

for (i in 2:4) {
  variable <- readRDS(paste("data/", location.names[i], sep = ""))
  
  profit <- variable %>% 
    mutate(
      revenue = map_dbl(sim.data, ~ sum(.$data$revenue[final.year.start:final.year.end])),
      tv.spend = map_dbl(sim.data, ~ sum(.$data$tv.spend[final.year.start:final.year.end])),
      search.spend = map_dbl(sim.data, ~ sum(.$data$search.spend[final.year.start:final.year.end])),
      search.sales = map_dbl(sim_search, ~ sum(.$revenue[final.year.start:final.year.end])),
      promo = map_dbl(sim_non, ~ sum(.$revenue[final.year.start:final.year.end])),
      search.incr = revenue - search.sales,
      tv.incr = search.sales - promo
    ) %>% 
    summarise(profit = sum(revenue - tv.spend - search.spend),
	      search.incr = sum(search.incr),
	      tv.incr = sum(tv.incr),
	      tv.spend = sum(tv.spend),
              search.spend = sum(search.spend),
	      tv.roas = tv.incr/tv.spend,
              search.roas = search.incr/search.spend)
  print(profit)
  profits[i] <- profit$profit
}

print(profits)